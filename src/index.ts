'use strict';
import './styles/main.scss';
import Canvas from './canvas/Canvas';
import TextElement from './canvas/TextElement';
import Point2D from './map/Point2D';
import RoomFloorNode from './canvas/nodes/RoomFloorNode';
declare global {
	interface Window {
		debug:any[];
	}
}

window.addEventListener('load', function init() {
	const game = new GameClient();
	game.init();
});

class GameClient {
	private canvas: Canvas;
	private timer: DrawTimer;
	private texts: TextElement[];
	//private roomFloor: RoomFloorNode;
	
	constructor() {
		this.canvas = new Canvas(document.createElement('canvas'));
		this.timer = new DrawTimer((time) => this.render());
		this.timer.start();
	

		//this.roomFloor = this.canvas.appendElement(new RoomFloorNode(this.canvas, new Point2D(20, 20)));
		//this.roomFloor.generateMap();

		this.texts = [
			this.canvas.appendElement(new TextElement(this.canvas, new Point2D(10, 20), "lime", "2em 'Fira Code'")),
			this.canvas.appendElement(new TextElement(this.canvas, new Point2D(10, 35), "lime", "1em 'Fira Code'")),
			this.canvas.appendElement(new TextElement(this.canvas, new Point2D(10, 45), "lime", ".5em 'Fira Code'"))
		];
		
		window.debug = [this.texts, this.canvas];
		
		
	}
	
	init() {
		document.body.appendChild(this.canvas.getElement());
		this.resizeToWindow();
	}
	
	resizeToWindow() {
		this.canvas.resize(window.innerWidth, window.innerHeight);
	}
	
	render() {
		this.canvas.clear();
		this.texts[0].setValue(`FPS: ${this.timer.getFps().getFps()} (${this.timer.getFps().getFramesInTheSecond()}/${this.timer.getFps().getLastFrameRate()})`);
		this.texts[1].setValue(`[${this.timer.getFps().getLastFrameRates().join(",")}]`);
		this.texts[2].setValue(`${this.canvas.getElements().length} elements`);
		this.canvas.render();
	}
	
	
}

class DrawTimer {
	private callback: FrameRequestCallback;
	private running: boolean;
	private lastCall: number;
	private fps:FramePerSecondChecker;
	
	constructor(callback: FrameRequestCallback) {
		this.callback = callback;
		this.fps = new FramePerSecondChecker();
	}
	
	getLastCall() {
		return this.lastCall;
	}
	
	start() {
		this.doTick();
		this.running = true;
	}
	
	doTick() {
		this.lastCall = TimeUtils.now();
		this.fps.handleTick();
		window.requestAnimationFrame((time) => this.tick(time));
	}
	
	tick(tickTime: number):void {
		this.callback(tickTime);
		
		if (this.running) {
			this.doTick();
		}
	}
	
	stop() {
		this.running = false;
	}
	
	getFps(){
		return this.fps;
	}
}

class FramePerSecondChecker {
	private framesInTheSecond:number;
	private actualSeconds:number;
	private lastFrameRates: number[];
	private lastFrameRate: number;
	private frameAverage: number;
	
	constructor() {
		this.framesInTheSecond = 0;
		this.actualSeconds = 0;
		this.lastFrameRates = [];
		this.frameAverage = 3;
	}

	getLastFrameRates() {
		return this.lastFrameRates;
	}

	getLastFrameRate() {
		return this.lastFrameRate;
	}

	getFramesInTheSecond() {
		return this.framesInTheSecond;
	}
	
	handleTick() {
		if (this.actualSeconds === TimeUtils.now()) {
			this.framesInTheSecond++;
		} else {
			this.lastFrameRate = this.framesInTheSecond;
			this.lastFrameRates.push(this.framesInTheSecond);
			this.framesInTheSecond = 1;
			this.actualSeconds = TimeUtils.now();
			while (this.lastFrameRates.length > this.frameAverage) {
				this.lastFrameRates.shift();
			}
		}
	}
	
	getFps() {
		const total = this.lastFrameRates.reduce((prev, curr) => prev + curr);
		return Math.round(total / this.lastFrameRates.length);
	}
}

class TimeUtils {
	static now() {
		return Math.floor(Date.now() / 1000);
	}
}
