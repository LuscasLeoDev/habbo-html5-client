import CanvasNode from "./CanvasNode";

export default class Canvas {
	private canvas:HTMLCanvasElement;	
	private context: CanvasRenderingContext2D;
	private nodes: CanvasNode[];

	constructor(canvas: HTMLCanvasElement){
		this.canvas = canvas;
		this.context = this.canvas.getContext('2d');
		this.nodes = [];
	}

	public appendElement<E extends CanvasNode>(canvas: E): E{
		this.nodes.push(canvas);
		return canvas;
	}

	resize ( width: number, height: number ) {
		this.canvas.width = width;
		this.canvas.height = height;
	}

	clear() {
		this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
	}

	getElement(): HTMLCanvasElement{
		return this.canvas;
	}

	getElements() {
		return this.nodes;
	}

	getContext(){
		return this.context;
	}

	render(){
		for (const element of this.nodes) {
			this.renderNode(element);
		}
	}

	renderNode(node: CanvasNode) {
		node.render(this.context);
		for (const child of node.getChildren()) {
			this.renderNode(child);
		}
	}
}