import Canvas from "./Canvas";
import Point2D from "../map/Point2D";
import CanvasNode from "./CanvasNode";

export default abstract class DefaultCanvasNode implements CanvasNode {
	protected position: Point2D;
	protected nodes: CanvasNode[];
	protected canvas: Canvas;

	constructor(canvas: Canvas, position: Point2D) {
		this.canvas = canvas;
		this.position = position;
		this.nodes = [];
	}

	getPosition() {
		return this.position;
	}
	
	abstract getHeight(): number;
	abstract getWidth(): number;

	abstract render(ctx: CanvasRenderingContext2D): void;

	abstract isInBounds(position: Point2D): boolean;

	getChildren() {
		return this.nodes;
	}
}