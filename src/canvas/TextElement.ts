import CanvasNode from "./CanvasNode";
import Canvas from "./Canvas";
import Point2D from "../map/Point2D";

export default class TextElement implements CanvasNode {
	private position: Point2D;
	private value: string;
	private canvas: Canvas;
	private textProps: TextMetrics;
	private style: string;
	private font: string;

	constructor(canvas: Canvas,position: Point2D, style: string, font: string = "10px serif") {
		this.canvas = canvas;
		this.value = "";
		this.textProps = null;
		this.style = style;
		this.font = font;
		this.position = position;
	}

	render ( ctx: CanvasRenderingContext2D ): void {
		if(this.textProps == null)
			return;
		ctx.fillStyle = this.style;
		ctx.font = this.font;
		ctx.fillText(this.value, this.position.getX(), this.position.getY() + this.textProps.actualBoundingBoxAscent);
	}

	getPosition() {
		return this.position;
	}

	getWidth():number {
		return this.textProps.width;
	}

	getHeight():number {
		return this.textProps.actualBoundingBoxAscent;
	}

	public setValue(v : string) {
		this.value = v;
		this.updateElementData();
	}

	private updateElementData(){
		this.textProps = this.canvas.getContext().measureText(this.value);
	}

	isInBounds(position: Point2D) {
		return this.position.getX() < position.getX() && this.position.getX() + this.getWidth() > position.getX()
		&& this.position.getY() < position.getY() && this.position.getY() + this.getHeight() > position.getY();
	}

	getChildren(): CanvasNode[] {
		return [];
	}

}