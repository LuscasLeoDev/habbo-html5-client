import Canvas from "./Canvas";
import Point2D from "../map/Point2D";

export default interface CanvasNode {
	render(ctx: CanvasRenderingContext2D): void;
		
	getPosition(): Point2D;
	getWidth(): number;
	getHeight(): number;

	isInBounds(position: Point2D): boolean;

	getChildren(): CanvasNode[];
}