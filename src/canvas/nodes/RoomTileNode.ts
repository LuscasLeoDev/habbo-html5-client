import DefaultCanvasNode from "../DefaultCanvasNode";
import Point2D from "../../map/Point2D";
import Canvas from "../Canvas";
import { FloorTileColor, TileColors } from "./FloorTileColor";

export default class RoomTileElement extends DefaultCanvasNode {

	private tileSize: Point2D;
	private tileTickness: number;
	public tileColor: FloorTileColor;

	constructor(canvas: Canvas, position: Point2D, thickness: number, size: Point2D = new Point2D(64, 32)) {
		super(canvas, position);
		this.tileSize = size;
		this.tileTickness = thickness;
		this.tileColor = TileColors.pink;
	}

	getHeight(): number {
		return this.tileSize.getX();
	}
	getWidth(): number {
		return this.tileSize.getY();
	}
	render( ctx: CanvasRenderingContext2D ): void {
		this.drawTile(ctx, this.position.getX(), this.position.getY(), true, true);
	}
	init( canvas: Canvas ): void {
	}

	isInBounds( position: Point2D ): boolean {
		return this.position.getX() < position.getX() && this.position.getX() + this.getWidth() > position.getX()
			&& this.position.getY() < position.getY() && this.position.getY() + this.getHeight() > position.getY();
	}

	drawTile(ctx: CanvasRenderingContext2D, x: number, y: number, bridgeY: boolean, bridgeX: boolean) {
		
		let startX = x + this.tileSize.getX() / 2;
		let startY = y;
	
	
		let points = [];
		points.push({ x: x + this.tileSize.getX(), y: y + this.tileSize.getY() / 2 })
		points.push({ x: x + this.tileSize.getX() / 2, y: y + this.tileSize.getY() })
		points.push({ x: x, y: y + this.tileSize.getY() / 2 })
	
	
		ctx.fillStyle = this.tileColor.primary;
		ctx.strokeStyle = this.tileColor.primaryStroke;
		ctx.lineWidth = 1;
	
		ctx.beginPath();
		ctx.moveTo(startX, startY);
		points.forEach(p => {
		  ctx.lineTo(p.x, p.y)
		});
		ctx.lineTo(startX, startY);
		ctx.fill();
		ctx.stroke();
	
		ctx.closePath();
	
	
		{
		  if (bridgeX) {
			points = [];
			ctx.fillStyle = this.tileColor.frontColor;
			ctx.strokeStyle = this.tileColor.frontStroke;
			ctx.lineWidth = 1;
			points.push({ x: x + this.tileSize.getX() / 2, y: y + this.tileSize.getY() })
			points.push({ x: x + this.tileSize.getX() / 2, y: y + this.tileSize.getY() + this.tileTickness })
			points.push({ x: x, y: y + this.tileSize.getY() / 2 + this.tileTickness })
			ctx.beginPath();
			ctx.moveTo(x, y + this.tileSize.getY() / 2);
			points.forEach(p => {
			  ctx.lineTo(p.x, p.y)
			});
			ctx.lineTo(x, y + this.tileSize.getY() / 2);
	
			ctx.fill();
			ctx.stroke();
			ctx.closePath();
		  }
	
	
		  if (bridgeY) {
	
			points = [];
			ctx.fillStyle = this.tileColor.sideColor;
			ctx.strokeStyle = this.tileColor.sideStroke;
			ctx.lineWidth = 1;
			points.push({ x: x + this.tileSize.getX(), y: y + this.tileSize.getY() / 2 })
			points.push({ x: x + this.tileSize.getX(), y: y + this.tileSize.getY() / 2 + this.tileTickness })
			points.push({ x: x + this.tileSize.getX() / 2, y: y + this.tileSize.getY() + this.tileTickness })
			ctx.beginPath();
			ctx.moveTo(x + this.tileSize.getX() / 2, y + this.tileSize.getY());
			points.forEach(p => {
			  ctx.lineTo(p.x, p.y)
			});
			ctx.lineTo(x + this.tileSize.getX() / 2, y + this.tileSize.getY());
			ctx.fill();
			ctx.stroke();
		  }
		}
	
	
	  }

}