export interface FloorTileColor {
	readonly primary: string;
	readonly primaryStroke: string;

	readonly frontColor: string;
	readonly frontStroke: string;

	readonly sideColor: string;
	readonly sideStroke: string;
}

export const TileColors =  {
	default : <FloorTileColor>{
		primary: "#989865",
		primaryStroke: "#8E8E5E",

		frontColor :"#838357",
		frontStroke: "#7A7A51",

		sideColor : "#6F6F49",
		sideStroke: "#676744"
	},
	 
	pink : <FloorTileColor>{
		primary: "#ccac98",
		primaryStroke: "#c7a795",

		frontColor :"#c1a291",
		frontStroke: "#bd9d8e",

		sideColor : "#b7988a",
		sideStroke: "#b39488",
	}

}