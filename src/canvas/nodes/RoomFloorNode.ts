import DefaultCanvasNode from "../DefaultCanvasNode";
import Point2D from "../../map/Point2D";
import RoomTileNode from "./RoomTileNode";
import Canvas from "../Canvas";
import TextElement from "../TextElement";
import { TileColors } from "./FloorTileColor";

export default class RoomFloorElement extends DefaultCanvasNode {
	private mapWidth: number = 10;
	private mapLength: number = 8;
	private txt: TextElement;
	private map: number[] = [
		1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1
	];

	constructor(canvas: Canvas, position: Point2D){
		super(canvas, position);
	}

	generateMap(){
		let lowestX = 0;
		let lowestY = 0;
		let highestX = 0;
		let highestY = 0;

		for (let x = 0; x < this.mapWidth; x++) {
			for (let y = 0; y < this.mapLength; y++) {
				// const tilePoint = this.map[x + y * this.mapLength];

				let offsetX = (x - y) * 64 / 2;
				let offsetY = (x + y) * 32 / 2;

				if(highestY < offsetY)
					highestY = offsetY;

				if(lowestX > offsetX)
					lowestX = offsetX;
			}
		}

		for (let x = 0; x < this.mapWidth; x++) {
			for (let y = 0; y < this.mapLength; y++) {

				let offsetX = (x - y) * 64 / 2;
				let offsetY = (x + y) * 32 / 2;

				this.nodes.push(new RoomTileNode(this.canvas, new Point2D(
					offsetX - lowestX,
					offsetY
					), 10));
			}
		}
	}

	getHeight(): number {
		return 100;
	}

	getWidth(): number {
		return 100;
	}

	render( ctx: CanvasRenderingContext2D ): void {
		
	}

	init( canvas: Canvas ): void {
		this.nodes.unshift(this.txt = canvas.appendElement(new TextElement(this.canvas, new Point2D(10, 0), "skyblue", "1em Fira Code")));
		this.txt.setValue("Mouse Pos");
		this.txt.getPosition().setY(window.innerHeight - this.txt.getHeight() - 10);
		canvas.getElement().addEventListener('mousemove', (e) => {	
			this.txt.setValue(`clientX => ${e.clientX} | clientY => ${e.clientY}`);

			const clientOffset = new Point2D(e.clientX, e.clientY);
			for (const child of this.nodes) {
				 if(!(child instanceof RoomTileNode))
					continue;

				const tile = child as (RoomTileNode);

				if(tile.isInBounds(clientOffset)){
					tile.tileColor = TileColors.pink;
				} else {
					tile.tileColor = TileColors.default;
				}

			}
		});
	}

	isInBounds( position: Point2D ): boolean {
		return true;
	}


}