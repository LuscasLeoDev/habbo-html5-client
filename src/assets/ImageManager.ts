

export default class ImageManager {
	
	private images: Map<String, HTMLImageElement>;

	constructor() {
		this.images = new Map();
	}

	getOrLoadImage(url: string): Promise<HTMLImageElement> {
		if(!this.images.has(url))
			return this.loadImage(url);
		
		return Promise.resolve(this.images.get(url));
	}

	loadImage(url: string): Promise<HTMLImageElement> {
		const img = new Image();

		return new Promise<HTMLImageElement>((rs, rj) => {
			img.addEventListener('load', () => {
				this.store(url, img);
				rs(img);
			});

			img.addEventListener('error', (e) => {
				rj(e);
			})

			img.src = url;
		});
	}

	store(url:string, img: HTMLImageElement) {
		this.images.set(url, img);
	}


}