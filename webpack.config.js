var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');

module.exports = {
	entry: "./src/index.ts",
	module : {
		rules : [
			{
				test: /\.tsx?$/,
				exclude: /(node_modules|bower_components)/,
				use: 'ts-loader'
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
				  // Creates `style` nodes from JS strings
				  'style-loader',
				  // Translates CSS into CommonJS
				  'css-loader',
				  // Compiles Sass to CSS
				  'sass-loader',
				],
			  },
		]
	},
	resolve : {
		extensions: ['.tsx', '.ts', '.js']
	},
	output: {
		path: path.resolve(__dirname, './dist'),
		filename: 'bundle.js'
	},
	devtool: 'inline-source-map',
	plugins: [
		new HtmlWebpackPlugin()
	],
	mode: 'development',
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		compress: true,
		port: 9000
	}
};